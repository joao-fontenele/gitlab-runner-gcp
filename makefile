
all: run

init:
	terraform init

run: init
	terraform plan

apply:
	terraform apply

destroy:
	terraform destroy