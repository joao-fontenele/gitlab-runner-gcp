variable "region" {
  description = "GCP region to create resources"
  type = string
  default = "us-east1" # https://gcping.com/
}

variable "zone" {
  description = "GCP zone to create resources"
  type = string
  default = "us-east1-c"
}

variable "project" {
  description = "Which GCP project to create resources in"
  type = string
  default = "terraform-test-jp"
}

variable "credentials_path" {
  description = "Path to GCP account credential file"
  type = string
  default = "./credential-gcp.json"
}

variable "instance_type" {
  description = "Which GCP instance type should be created"
  type = string
  default = "e2-micro" # free tier
}

variable "vpc_name" {
  description = "Name of the VPC where the gitlab runner will run"
  type = string
  default = "gitlab-runner"
}