terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "~> 3.59.0"
    }
  }
}

provider "google" {
  credentials = file(var.credentials_path)
  project = var.project
  region = var.region
  zone = var.zone
}

resource "google_compute_network" "gitlab_runner_vpc" {
  name = var.vpc_name
  auto_create_subnetworks = false
  delete_default_routes_on_create = true
  routing_mode = "GLOBAL"
}

resource "google_compute_subnetwork" "gitlab_runner_subnet" {
  name = "${var.vpc_name}-subnet"
  ip_cidr_range = "10.1.0.0/16"
  network = google_compute_network.gitlab_runner_vpc.self_link
}

resource "google_compute_instance" "gitlab_runner" {
  name = "gitlab-runner"
  machine_type = var.instance_type

  tags = ["terraform", "ci", "gitlab", "runner"]
  labels = {
    created_by = "terraform"
    environment = "production"
  }

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.gitlab_runner_subnet.self_link
  }
}
